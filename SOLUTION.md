# Solution

The application is written using CQRS architectural pattern. It means that every action is divided on query or command and handler.
That helped me to create universal RetrieveDataFromExternalSourceQueryHandler which depends on ExternalSourceDataProvider.
Different queries can be routed to this handler with injected different implementations of ExternalSourceDataProvider.
Handler supports caching that will be utilised automatically during calling other sources.

I haven't used any database so far. I have prepared required interfaces to store and read retrieved data.
I also have implemented simple JsonFileStorage needed while development process. That makes easy to use any storage in the future.
Sorting has been implemented in code and is ready to extend or use it as is.

I would recommend to use some schemaless database like MongoDB.
Every result of search could be a document in single collection.
There is no problem with migrations and supporting different structures of search result.

Extending this application to support different external platforms like Stack Overflow or LinkedIn is quite easy.
We can create eg. StackOverflowQuestionsQuery and route it to RetrieveDataFromExternalSourceQueryHandler with injected some StackOverflowQuestionsProvider.
If we want to provide specific sorting we have to create suitable comparators. Like is done in Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors\ByContributions.
There are also a few configuration and DI container parameters to fix or add.