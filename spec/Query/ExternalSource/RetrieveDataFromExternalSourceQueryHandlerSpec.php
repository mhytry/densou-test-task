<?php

namespace spec\Densou\TradingDesk\Query\ExternalSource;

use Densou\TradingDesk\Query\CacheProvider;
use Densou\TradingDesk\Query\ExternalSource\ExternalSourceDataProvider;
use Densou\TradingDesk\Query\ExternalSource\RetrieveDataFromExternalSourceQuery;
use Densou\TradingDesk\Query\ExternalSource\RetrieveDataFromExternalSourceQueryHandler;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use React\Promise\Deferred;

class RetrieveDataFromExternalSourceQueryHandlerSpec extends ObjectBehavior
{

    public function let(
        ExternalSourceDataProvider $repositoryContributorsProvider,
        CacheProvider $cacheProvider
    ) {
        $this->beConstructedWith($repositoryContributorsProvider, $cacheProvider);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(RetrieveDataFromExternalSourceQueryHandler::class);
    }

    public function it_does_not_call_provider_if_cache_contains_querying_data(
        ExternalSourceDataProvider $repositoryContributorsProvider,
        CacheProvider $cacheProvider,
        RetrieveDataFromExternalSourceQuery $query,
        Deferred $deferred
    ) {

        $hash = 'hash';

        $query->hash()->willReturn($hash);
        $cacheProvider->get($hash)->willReturn([]);

        $repositoryContributorsProvider->data(Argument::any())->shouldNotBeCalled();

        $this->__invoke($query, $deferred);
    }

    public function it_calls_provider_and_sets_cache_if_cache_does_not_contain_querying_data(
        ExternalSourceDataProvider $repositoryContributorsProvider,
        CacheProvider $cacheProvider,
        RetrieveDataFromExternalSourceQuery $query,
        Deferred $deferred
    ) {

        $hash = 'hash';
        $subject = 'test subject';

        $query->hash()->willReturn($hash);
        $query->subject()->willReturn($subject);
        $cacheProvider->get($hash)->willReturn(null);

        $repositoryContributorsProvider->data($subject)->shouldBeCalled();

        $repositoryContributorsProvider->data($subject)->willReturn([]);
        $cacheProvider->set($hash, [])->shouldBeCalled();

        $this->__invoke($query, $deferred);
    }
}
