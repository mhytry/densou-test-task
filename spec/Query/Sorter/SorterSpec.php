<?php

namespace spec\Densou\TradingDesk\Query\Sorter;

use Densou\TradingDesk\Query\Sorter\Comparator;
use Densou\TradingDesk\Query\Sorter\Direction;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SorterSpec extends ObjectBehavior
{


    public function it_does_not_change_order_if_data_is_sorted(Comparator $comparator)
    {

        $comparator->compare(Argument::any(), Argument::any())->willReturn(1);
        $direction = Direction::ASC();

        $this->beConstructedWith($comparator, $direction);

//        $data = [1, 2, 3];
//        $sorted = [1, 2, 3];
//
//        $this->sort($sorted);

//        Assertion::same($data, $sorted);


        //can't run test, phpspec can't handle pass variable as a reference
    }
}
