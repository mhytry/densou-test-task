<?php

namespace spec\Densou\TradingDesk\Query\Sorter\Comparator;

use Densou\TradingDesk\Query\Sorter\Comparator\Number;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NumberSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Number::class);
    }

    public function it_returns_negative_if_firts_argument_is_less_then_second()
    {
        $this->compare(1 ,2)->shouldBeNegative();
    }

    public function it_returns_positive_if_firts_argument_is_greater_then_second()
    {
        $this->compare(2, 1)->shouldBePositive();
    }

    public function it_returns_0_if_firts_argument_is_the_same_as_second()
    {
        $this->compare(2, 2)->shouldBeZero();
    }

    public function getMatchers(): array
    {
        return [
            'beNegative' => function ($subject) {
                return $subject < 0;
            },
            'bePositive' => function ($subject) {
                return $subject > 0;
            },
            'beZero' => function ($subject) {
                return $subject === 0;
            }
        ];
    }
}
