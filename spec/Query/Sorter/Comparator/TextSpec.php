<?php

namespace spec\Densou\TradingDesk\Query\Sorter\Comparator;

use Densou\TradingDesk\Query\Sorter\Comparator\Text;
use PhpSpec\ObjectBehavior;

class TextSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Text::class);
    }

    public function it_returns_negative_if_firts_argument_is_less_then_second()
    {
        $this->compare('a' ,'b')->shouldBeNegative();
    }

    public function it_returns_positive_if_firts_argument_is_greater_then_second()
    {
        $this->compare('b', 'a')->shouldBePositive();
    }

    public function it_returns_0_if_firts_argument_is_the_same_as_second()
    {
        $this->compare('a', 'a')->shouldBeZero();
    }

    public function getMatchers(): array
    {
        return [
            'beNegative' => function ($subject) {
                return $subject < 0;
            },
            'bePositive' => function ($subject) {
                return $subject > 0;
            },
            'beZero' => function ($subject) {
                return $subject === 0;
            }
        ];
    }
}
