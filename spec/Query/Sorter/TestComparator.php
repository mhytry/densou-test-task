<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 01.10.2017
 * Time: 23:49
 */

namespace spec\Densou\TradingDesk\Query\Sorter;


use Densou\TradingDesk\Query\Sorter\Comparator;

class TestComparator implements Comparator {

    public function compare($item, $nextItem): int
    {
        return 0;
    }
}