<?php

namespace spec\Densou\TradingDesk\Query\Sorter;

use Densou\TradingDesk\Query\Sorter\Direction;
use Densou\TradingDesk\Query\Sorter\Order;
use Densou\TradingDesk\Query\Sorter\Sorter;
use Densou\TradingDesk\Query\Sorter\SorterFactory;
use Densou\TradingDesk\Query\Sorter\UnknownSorterException;
use PhpSpec\ObjectBehavior;

class SorterFactorySpec extends ObjectBehavior
{

    public function let()
    {

        $sorters = [
            'test' => TestComparator::class
        ];

        $this->beConstructedWith($sorters);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(SorterFactory::class);
    }

    public function it_throws_UnknownSorterException_if_requested_sorter_is_not_in_sorters(Order $order)
    {
        $order->by()->willReturn('unknown-sorter');
        $this->shouldThrow(UnknownSorterException::class)->during('create', [$order]);
    }

    public function it_returns_sorter_with_corresponding_comparator(Order $order)
    {
        $order->by()->willReturn('test');
        $order->direction()->willReturn(Direction::ASC());

        $sorter = $this->create($order);

        $sorter->shouldBeAnInstanceOf(Sorter::class);
    }
}
