<?php

namespace spec\Densou\TradingDesk\Query\SearchHistory;

use Densou\TradingDesk\Query\SearchHistory\RecentSearchesQuery;
use Densou\TradingDesk\Query\SearchHistory\RecentSearchesQueryHandler;
use Densou\TradingDesk\Query\SearchHistory\SearchHistoryReaderProvider;
use PhpSpec\ObjectBehavior;
use React\Promise\Deferred;

class RecentSearchesQueryHandlerSpec extends ObjectBehavior
{

    public function let(SearchHistoryReaderProvider $searchHistoryProvider)
    {
        $this->beConstructedWith($searchHistoryProvider);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(RecentSearchesQueryHandler::class);
    }

    public function it_removes_search_result_from_recent_searches(
        SearchHistoryReaderProvider $searchHistoryProvider,
        RecentSearchesQuery $query,
        Deferred $deferred
    ) {

        $limit = 5;

        $query->limit()->willReturn($limit);

        $searchHistoryProvider->last($limit)->willReturn($this->data());
        $deferred->resolve($this->modifiedData())->shouldBeCalled();

        $this->__invoke($query, $deferred);
    }

    private function data(): array
    {
        return [
            [
                'query' => 'phalcon/cphalcon',
                'result' => []
            ],
            [
                'query' => 'symfony/symfony',
                'result' => []
            ]
        ];
    }

    private function modifiedData(): array
    {
        return [
            [
                'query' => 'phalcon/cphalcon'
            ],
            [
                'query' => 'symfony/symfony'
            ]
        ];
    }
}
