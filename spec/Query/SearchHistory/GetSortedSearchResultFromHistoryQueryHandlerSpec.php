<?php

namespace spec\Densou\TradingDesk\Query\SearchHistory;

use Densou\TradingDesk\Query\SearchHistory\GetSortedSearchResultFromHistoryQuery;
use Densou\TradingDesk\Query\SearchHistory\GetSortedSearchResultFromHistoryQueryHandler;
use Densou\TradingDesk\Query\SearchHistory\SearchHistoryReaderProvider;
use Densou\TradingDesk\Query\Sorter\Direction;
use Densou\TradingDesk\Query\Sorter\Order;
use Densou\TradingDesk\Query\Sorter\Sorter;
use Densou\TradingDesk\Query\Sorter\SorterFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;
use React\Promise\Deferred;

class GetSortedSearchResultFromHistoryQueryHandlerSpec extends ObjectBehavior
{

    public function let(SearchHistoryReaderProvider $searchHistoryProvider, SorterFactory $sorterFactory)
    {
        $this->beConstructedWith($searchHistoryProvider, $sorterFactory);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(GetSortedSearchResultFromHistoryQueryHandler::class);
    }

    public function it_returns_not_modified_data_if_order_is_not_specified(
        SearchHistoryReaderProvider $searchHistoryProvider,
        GetSortedSearchResultFromHistoryQuery $query,
        Deferred $deferred
    ) {

        $searchId = Uuid::uuid4();

        $query->searchId()->willReturn($searchId);
        $query->order()->willReturn(null);

        $searchHistoryProvider->get($searchId)->willReturn($this->data());

        $deferred->resolve($this->data())->shouldBeCalled();

        $this->__invoke($query, $deferred);
    }

    public function it_calls_sort_data_if_order_is_specified(
        SearchHistoryReaderProvider $searchHistoryProvider,
        GetSortedSearchResultFromHistoryQuery $query,
        Deferred $deferred,
        SorterFactory $sorterFactory,
        Sorter $sorter
    ) {
        $searchId = Uuid::uuid4();
        $order = new Order('name', Direction::ASC());

        $query->searchId()->willReturn($searchId);
        $query->order()->willReturn($order);

        $searchHistoryProvider->get($searchId)->willReturn($this->data());

        $sorterFactory->create($order)->willReturn($sorter);

        $sorter->sort($this->data()['result'])->shouldBeCalled();

        $this->__invoke($query, $deferred);
    }

    private function data(): array
    {
        return [
            'result' => [
                1, 2, 3
            ]
        ];
    }
}
