<?php

namespace spec\Densou\TradingDesk\Command\SearchHistory;

use DateTimeImmutable;
use Densou\TradingDesk\Command\DateTimeProvider;
use Densou\TradingDesk\Command\SearchHistory\AddToSearchHistoryCommand;
use Densou\TradingDesk\Command\SearchHistory\AddToSearchHistoryCommandHandler;
use Densou\TradingDesk\Command\SearchHistory\SearchHistoryWriterProvider;
use Densou\TradingDesk\Model\SearchHistoryItem;
use PhpSpec\ObjectBehavior;
use Ramsey\Uuid\Uuid;

class AddToSearchHistoryCommandHandlerSpec extends ObjectBehavior
{

    function let(SearchHistoryWriterProvider $searchHistoryWriterProvider, DateTimeProvider $dateTimeProvider)
    {
        $this->beConstructedWith($searchHistoryWriterProvider, $dateTimeProvider);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AddToSearchHistoryCommandHandler::class);
    }

    function it_will_save_SearchHistoryItem(
        SearchHistoryWriterProvider $searchHistoryWriterProvider,
        AddToSearchHistoryCommand $command,
        DateTimeProvider $dateTimeProvider
    ) {

        $current = new DateTimeImmutable();

        $dateTimeProvider->current()->willReturn($current);

        $testSearchHistoryItemObject = new SearchHistoryItem(
            Uuid::uuid4(), 'phalcon/cphalcon', [], $current
        );

        $command->searchId()->willReturn($testSearchHistoryItemObject->searchId());
        $command->searchText()->willReturn($testSearchHistoryItemObject->searchText());
        $command->searchResult()->willReturn($testSearchHistoryItemObject->searchResult());

        $searchHistoryWriterProvider->add($testSearchHistoryItemObject)->shouldBeCalled();

        $this->__invoke($command);

    }
}
