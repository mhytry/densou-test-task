<?php

use Densou\TradingDesk\Infrastructure\SearchHistory\JsonFileSearchHistoryReaderProvider;
use Densou\TradingDesk\Infrastructure\SearchHistory\JsonFileSearchHistoryWriterProvider;

require_once 'prod.php';

$app['app.provider.search_history_reader'] = function () {
    return new JsonFileSearchHistoryReaderProvider(__DIR__ . '/../../var/json_storage/recent_search.json');
};

$app['app.provider.search_history_writer'] = function () {
    return new JsonFileSearchHistoryWriterProvider(__DIR__ . '/../../var/json_storage/recent_search.json');
};
