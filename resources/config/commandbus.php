<?php

use Densou\TradingDesk\Command\SearchHistory\AddToSearchHistoryCommand;
use Densou\TradingDesk\Command\SearchHistory\AddToSearchHistoryCommandHandler;
use Densou\TradingDesk\Infrastructure\LocalDateTimeProvider;
use Pimple\Container;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;

$app['commandbus'] = function (Container $container) {

    $commandBus = new CommandBus();
    $commandRouter = new CommandRouter();

    $commandRouter->attachToMessageBus($commandBus);

    // Routing
    $commandRouter
        ->route(AddToSearchHistoryCommand::class)
        ->to(
            new AddToSearchHistoryCommandHandler(
                $container['app.provider.search_history_writer'],
                new LocalDateTimeProvider()
            )
        );

    return $commandBus;
};