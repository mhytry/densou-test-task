<?php

use Densou\TradingDesk\Query\SearchHistory\GetSortedSearchResultFromHistoryQuery;
use Densou\TradingDesk\Query\SearchHistory\GetSortedSearchResultFromHistoryQueryHandler;
use Densou\TradingDesk\Query\SearchHistory\RecentSearchesQueryHandler;
use Densou\TradingDesk\Query\SearchHistory\RecentSearchesQuery;
use Densou\TradingDesk\Query\ExternalSource\Github\GithubRepositoryContributorsQuery;
use Densou\TradingDesk\Query\ExternalSource\RetrieveDataFromExternalSourceQueryHandler;
use Densou\TradingDesk\Query\Sorter\SorterFactory;
use Pimple\Container;
use Prooph\ServiceBus\Plugin\Router\QueryRouter;
use Prooph\ServiceBus\QueryBus;

$app['querybus'] = function (Container $container) {

    $queryBus = new QueryBus();
    $queryRouter = new QueryRouter();

    $queryRouter->attachToMessageBus($queryBus);

    // Routing
    $queryRouter
        ->route(RecentSearchesQuery::class)
        ->to(
            new RecentSearchesQueryHandler(
                $container['app.provider.search_history_reader']
            )
        )->route(GithubRepositoryContributorsQuery::class)
        ->to(
            new RetrieveDataFromExternalSourceQueryHandler(
                $container['app.provider.github_repository_contributors'],
                $container['app.provider.cache']
            )
        )->route(GetSortedSearchResultFromHistoryQuery::class)
        ->to(
            new GetSortedSearchResultFromHistoryQueryHandler(
                $container['app.provider.search_history_reader'],
                new SorterFactory($container['app.sorter.comparators']['repository-contrinutors'])
            )
        );

    return $queryBus;
};