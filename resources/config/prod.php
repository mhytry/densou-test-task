<?php

use Densou\TradingDesk\Infrastructure\CacheAdapter;
use Densou\TradingDesk\Infrastructure\RepositoryContributors\GithubContributorMapper;
use Densou\TradingDesk\Infrastructure\RepositoryContributors\GitHubRepositoryContributorsProvider;
use Densou\TradingDesk\Infrastructure\RepositoryContributors\GithubRepositoryNameSpecification;
use Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors\ByContributions;
use Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors\ByName;
use Github\Client;
use Github\ResultPager;
use Moust\Silex\Provider\CacheServiceProvider;
use Pimple\Container;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app->register(new CacheServiceProvider());
$app->register(new RoutingServiceProvider());
$app->register(new TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../views'
]);

$app['config'] = [
    'github.token' => getenv('GITHUB_TOKEN'),
    'recent_search.default_limit' => 5,
    'cache.default_ttl' => 60 * 60 * 24 //1 day
];

$app['cache.options'] = [
    'driver' => 'file',
    'cache_dir' => __DIR__ . '/../../var/cache'
];

$app['app.provider.search_history'] = "";

$app['app.provider.github_repository_contributors'] = function (Container $container) {

    $githubClient = new Client();
    $githubClient->authenticate(null, $container['config']['github.token'], null,
        Client::AUTH_HTTP_PASSWORD);


    return new GitHubRepositoryContributorsProvider(
        $githubClient, new ResultPager($githubClient), new GithubContributorMapper(),
        new GithubRepositoryNameSpecification()
    );
};

$app['app.provider.cache'] = function (Container $container) {
    return new CacheAdapter($container['cache'], $container['config']['cache.default_ttl']);
};

$app['app.sorter.comparators'] = [
    'repository-contrinutors' => [
        'contributions' => ByContributions::class,
        'name' => ByName::class
    ]
];