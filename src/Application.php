<?php

namespace Densou\TradingDesk;

use Symfony\Component\HttpFoundation\Response;

/**
 * Wrapper of Silex Application
 * @package Densou\TradingDesk
 */
class Application extends \Silex\Application {

    /**
     * Returns error response in json with provided message and code
     * @param string $message
     * @param int $code
     * @return Response
     */
    public function jsonError(string $message, int $code): Response
    {
        $code = in_array($code, array_keys(Response::$statusTexts)) ? $code : Response::HTTP_INTERNAL_SERVER_ERROR;

        return $this->json([
            'msg' => $message
        ], $code);
    }


}