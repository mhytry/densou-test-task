<?php

namespace Densou\TradingDesk\Query\SearchHistory;

use Densou\TradingDesk\Query\Sorter\SorterFactory;
use React\Promise\Deferred;

/**
 * Implementation of service to get sorted resultset of search query
 *
 * @package Densou\TradingDesk\Query\SearchHistory
 */
class GetSortedSearchResultFromHistoryQueryHandler
{

    /**
     * @var SearchHistoryReaderProvider
     */
    private $searchHistoryProvider;
    /**
     * @var SorterFactory
     */
    private $sorterFactory;

    /**
     * GetSortedSearchResultFromHistoryQueryHandler constructor.
     * @param SearchHistoryReaderProvider $searchHistoryProvider
     * @param SorterFactory $sorterFactory
     */
    public function __construct(SearchHistoryReaderProvider $searchHistoryProvider, SorterFactory $sorterFactory)
    {
        $this->searchHistoryProvider = $searchHistoryProvider;
        $this->sorterFactory = $sorterFactory;
    }

    /**
     * Returns history of search item with sorted search result
     * @param GetSortedSearchResultFromHistoryQuery $query
     * @param Deferred $deferred
     */
    public function __invoke(GetSortedSearchResultFromHistoryQuery $query, Deferred $deferred)
    {
        try {
            $historySearch = $this->searchHistoryProvider->get($query->searchId());

            if ($query->order()) {
                $sorter = $this->sorterFactory->create($query->order());
                $sorter->sort($historySearch['result']);
            }

            $deferred->resolve($historySearch);
        } catch (\Exception $e) {
            $deferred->reject($e);
        }
    }
}