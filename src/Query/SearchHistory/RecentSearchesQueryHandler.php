<?php

namespace Densou\TradingDesk\Query\SearchHistory;

use React\Promise\Deferred;

/**
 * Implementation of retrieveing last items from history of search
 *
 * @package Densou\TradingDesk\Query\SearchHistory
 */
class RecentSearchesQueryHandler
{

    /**
     * @var SearchHistoryReaderProvider
     */
    private $searchHistoryProvider;

    /**
     * RecentSearchesQueryHandler constructor.
     * @param SearchHistoryReaderProvider $searchHistoryProvider
     */
    public function __construct(SearchHistoryReaderProvider $searchHistoryProvider)
    {
        $this->searchHistoryProvider = $searchHistoryProvider;
    }

    /**
     * Returns N recent items from history of search and removes result of every search
     * @param RecentSearchesQuery $query
     * @param Deferred $deferred
     */
    public function __invoke(RecentSearchesQuery $query, Deferred $deferred): void
    {
        try {
            $recentSearches = $this->searchHistoryProvider->last($query->limit());

            array_walk($recentSearches, function (&$recentSearchItem) {
                unset($recentSearchItem['result']);
            });

            $deferred->resolve($recentSearches);
        } catch (\Exception $e) {
            $deferred->reject($e);
        }

    }
}