<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 29.09.2017
 * Time: 22:40
 */

namespace Densou\TradingDesk\Query\SearchHistory;

use Ramsey\Uuid\UuidInterface;

/**
 * Allows reading from history of searches
 *
 * @package Densou\TradingDesk\Query\SearchHistory
 */
interface SearchHistoryReaderProvider
{
    /**
     * Get common informations about last N items from history of searches
     * @param int $limit
     * @return array
     */
    public function last(int $limit): array;

    /**
     * Get particular item from history of searches
     * @param UuidInterface $searchId
     * @return array
     */
    public function get(UuidInterface $searchId): array;
}