<?php

namespace Densou\TradingDesk\Query\SearchHistory;

use Densou\TradingDesk\Query\Sorter\Order;
use Ramsey\Uuid\UuidInterface;

/**
 * DTO
 * @package Densou\TradingDesk\Query\SearchHistory
 */
class GetSortedSearchResultFromHistoryQuery
{

    /**
     * @var UuidInterface
     */
    private $searchId;
    /**
     * @var Order
     */
    private $order;

    /**
     * GetSortedSearchResultFromHistoryQuery constructor.
     * @param UuidInterface $searchId
     * @param Order|null $order
     */
    public function __construct(UuidInterface $searchId, Order $order = null)
    {

        $this->searchId = $searchId;
        $this->order = $order;
    }

    /**
     * @return UuidInterface
     */
    public function searchId(): UuidInterface
    {
        return $this->searchId;
    }

    /**
     * @return Order
     */
    public function order(): ?Order
    {
        return $this->order;
    }
}