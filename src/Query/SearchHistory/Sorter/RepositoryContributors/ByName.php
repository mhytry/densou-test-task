<?php

namespace Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors;

use Densou\TradingDesk\Query\Sorter\Comparator\Text;

/**
 * Compare names
 * @package Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors
 */
class ByName extends Text
{

    /**
     * @inheritdoc
     */
    public function compare($item, $nextItem): int
    {
        return parent::compare($item['name'], $nextItem['name']);
    }
}