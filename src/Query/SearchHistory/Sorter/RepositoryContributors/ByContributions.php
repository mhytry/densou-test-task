<?php

namespace Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors;

use Densou\TradingDesk\Query\Sorter\Comparator\Number;

/**
 * Compare contributions number
 * @package Densou\TradingDesk\Query\SearchHistory\Sorter\RepositoryContributors
 */
class ByContributions extends Number
{
    /**
     * @inheritdoc
     */
    public function compare($item, $nextItem): int
    {
        return parent::compare($item['contributions'], $nextItem['contributions']);
    }
}