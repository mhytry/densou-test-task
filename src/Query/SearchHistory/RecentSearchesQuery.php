<?php

namespace Densou\TradingDesk\Query\SearchHistory;

/**
 * DTO
 *
 * @package Densou\TradingDesk\Query\SearchHistory
 */
class RecentSearchesQuery
{
    /**
     * @var int
     */
    protected $limit;

    /**
     * RecentSearchQuery constructor.
     * @param $limit
     */
    public function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * Returns limit of recent searches
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }


}