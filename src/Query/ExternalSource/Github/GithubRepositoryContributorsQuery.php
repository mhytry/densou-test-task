<?php

namespace Densou\TradingDesk\Query\ExternalSource\Github;

use Densou\TradingDesk\Query\ExternalSource\RetrieveDataFromExternalSourceQuery;

/**
 * Customized object to keep search parameters
 * @package Densou\TradingDesk\Query\ExternalSource\Github
 */
class GithubRepositoryContributorsQuery extends RetrieveDataFromExternalSourceQuery
{

}