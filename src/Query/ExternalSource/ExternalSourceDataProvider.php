<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 12:10
 */

namespace Densou\TradingDesk\Query\ExternalSource;


/**
 * Provides mechanism to obtain data from external source like Github, StackOverflow, Linkedin, etc.
 *
 * @package Densou\TradingDesk\Query\ExternalSource
 */
interface ExternalSourceDataProvider
{
    /**
     * Returns data from external source
     *
     * @param string $subject
     * @return array
     */
    public function data(string $subject): array;
}