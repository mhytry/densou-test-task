<?php

namespace Densou\TradingDesk\Query\ExternalSource;

use Densou\TradingDesk\Query\CacheProvider;
use React\Promise\Deferred;

/**
 * Generic implementation of searching in external source and caching result
 *
 * @package Densou\TradingDesk\Query\ExternalSource
 */
class RetrieveDataFromExternalSourceQueryHandler
{

    /**
     * @var ExternalSourceDataProvider
     */
    private $externalSourceDataProvider;

    /**
     * @var CacheProvider
     */
    private $cacheProvider;

    /**
     * RetrieveDataFromExternalSourceQueryHandler constructor.
     * @param ExternalSourceDataProvider $externalSourceDataProvider
     * @param CacheProvider $cacheProvider
     */
    public function __construct(
        ExternalSourceDataProvider $externalSourceDataProvider,
        CacheProvider $cacheProvider
    ) {
        $this->externalSourceDataProvider = $externalSourceDataProvider;
        $this->cacheProvider = $cacheProvider;
    }

    /**
     * Retrieve data from external source
     * Supports caching
     *
     * @param RetrieveDataFromExternalSourceQuery $query
     * @param Deferred $deferred
     */
    public function __invoke(RetrieveDataFromExternalSourceQuery $query, Deferred $deferred): void
    {
        try {
            $retrievedData = $this->cacheProvider->get($query->hash());

            if (is_null($retrievedData)) {
                $retrievedData = $this->externalSourceDataProvider->data($query->subject());
                $this->cacheProvider->set($query->hash(), $retrievedData);
            }

            $deferred->resolve($retrievedData);
        } catch (\Exception $e) {
            $deferred->reject($e);
        }

    }

}