<?php

namespace Densou\TradingDesk\Query\ExternalSource;

use Densou\TradingDesk\Query\HashableQuery;

/**
 * DTO
 *
 * @package Densou\TradingDesk\Query\ExternalSource
 */
abstract class RetrieveDataFromExternalSourceQuery extends HashableQuery
{

    /**
     * @var string
     */
    protected $subject;

    /**
     * RetrieveDataFromExternalSourceQuery constructor.
     * @param string $subject
     */
    public function __construct(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * Returns query to search
     * @return string
     */
    public function subject(): string
    {
        return $this->subject;
    }

    /**
     * @inheritdoc
     */
    protected function payload(): array
    {
        return [
            'subject' => $this->subject
        ];
    }

}