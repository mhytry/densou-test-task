<?php

namespace Densou\TradingDesk\Query;

/**
 * Provides ability to calculate digest of object
 * @package Densou\TradingDesk\Query
 */
abstract class HashableQuery
{
    /**
     * Returns object's digest
     * @return string
     */
    public function hash(): string
    {
        return sha1(
            static::class . serialize($this->payload())
        );
    }

    /**
     * Returns object's parameters
     * @return array
     */
    abstract protected function payload(): array;
}