<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 20:23
 */

namespace Densou\TradingDesk\Query;


/**
 * Provides cache ability
 * @package Densou\TradingDesk\Query
 */
interface CacheProvider
{
    /**
     * Retrieve item from cache
     * @param string $key
     * @return array|null
     */
    public function get(string $key): ?array;

    /**
     * Add item to cache
     * @param string $key
     * @param array $data
     */
    public function set(string $key, array $data): void;
}