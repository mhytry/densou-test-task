<?php

namespace Densou\TradingDesk\Query\Sorter\Comparator;

use Densou\TradingDesk\Query\Sorter\Comparator;

/**
 * Compares 2 text values
 * @package Densou\TradingDesk\Query\Sorter\Comparator
 */
class Text implements Comparator
{

    /**
     * @inheritdoc
     */
    public function compare($item, $nextItem): int
    {
        return strcoll(mb_strtolower($item), mb_strtolower($nextItem));
    }
}