<?php

namespace Densou\TradingDesk\Query\Sorter\Comparator;

use Densou\TradingDesk\Query\Sorter\Comparator;

/**
 * Compares 2 number values
 * @package Densou\TradingDesk\Query\Sorter\Comparator
 */
class Number implements Comparator
{

    /**
     * @inheritdoc
     */
    public function compare($item, $nextItem): int
    {
        return $item - $nextItem;
    }
}