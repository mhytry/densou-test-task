<?php

namespace Densou\TradingDesk\Query\Sorter;

/**
 * Factory of sorter with proper implementation of comparator
 * @package Densou\TradingDesk\Query\Sorter
 */
class SorterFactory
{
    /**
     * @var array
     */
    private $sorters;

    /**
     * SorterFactory constructor.
     * @param array $sorters
     */
    public function __construct(array $sorters)
    {
        $this->sorters = $sorters;
    }

    /**
     * Creates sorter with specified comparator and direction
     * @param Order $order
     * @return Sorter
     *
     * @throws UnknownSorterException
     */
    public function create(Order $order): Sorter
    {
        if (!array_key_exists($order->by(), $this->sorters)) {
            throw new UnknownSorterException();
        }

        return new Sorter(
            new $this->sorters[$order->by()](),
            $order->direction()
        );
    }
}