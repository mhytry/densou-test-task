<?php

namespace Densou\TradingDesk\Query\Sorter;

/**
 * Object used in sorting process to compare 2 values
 * @package Densou\TradingDesk\Query\Sorter
 */
interface Comparator
{
    /**
     * Compares 2 values
     * @param $item
     * @param $nextItem
     * @return int Possible values: negative integer if $item < $nextItem, positive integer if $item > $nextItem, 0 if $item and $nextItem are equal,
     */
    public function compare($item, $nextItem): int;
}