<?php

namespace Densou\TradingDesk\Query\Sorter;

use MyCLabs\Enum\Enum;

/**
 * Enum values for sort directions
 * @package Densou\TradingDesk\Query\Sorter
 */
class Direction extends Enum
{
    /**
     * Ascending
     */
    const ASC = "ASC";

    /**
     * Descending
     */
    const DESC = "DESC";
}