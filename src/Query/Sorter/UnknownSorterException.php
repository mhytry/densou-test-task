<?php

namespace Densou\TradingDesk\Query\Sorter;

use OutOfBoundsException;

/**
 * Class UnknownSorterException
 * @package Densou\TradingDesk\Query\Sorter
 */
class UnknownSorterException extends OutOfBoundsException
{

    /**
     * UnknownSorterException constructor.
     */
    public function __construct()
    {
        parent::__construct("Unknown sorter");
    }
}