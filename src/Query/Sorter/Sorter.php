<?php

namespace Densou\TradingDesk\Query\Sorter;

/**
 * Implementation of sorter based on provided comparators and direction
 *
 * @package Densou\TradingDesk\Query\Sorter
 */
class Sorter
{
    /**
     * @var Comparator
     */
    private $comparator;
    /**
     * @var Direction
     */
    private $direction;

    /**
     * Sorter constructor.
     * @param Comparator $comparator
     * @param Direction $direction
     */
    public function __construct(Comparator $comparator, Direction $direction)
    {
        $this->comparator = $comparator;
        $this->direction = $direction;
    }

    /**
     * Sorts data as a reference using provided comparator in specified direction
     * @param array $data
     */
    public function sort(array &$data): void
    {
        usort($data, function ($toCompareA, $toCompareB) {

            return $this->direction->equals(Direction::ASC()) ?
                $this->comparator->compare($toCompareA, $toCompareB) :
                $this->comparator->compare($toCompareB, $toCompareA);
        });
    }

}