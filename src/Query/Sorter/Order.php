<?php

namespace Densou\TradingDesk\Query\Sorter;

/**
 * DTO
 * @package Densou\TradingDesk\Query\Sorter
 */
class Order
{
    /**
     * @var string
     */
    private $by;

    /**
     * @var Direction
     */
    private $direction;

    /**
     * Order constructor.
     * @param string $by
     * @param Direction $direction
     */
    public function __construct(string $by, Direction $direction)
    {
        $this->by = $by;
        $this->direction = $direction;
    }

    /**
     * @return Direction
     */
    public function direction(): Direction
    {
        return $this->direction;
    }

    /**
     * @return string
     */
    public function by(): string
    {
        return $this->by;
    }

}