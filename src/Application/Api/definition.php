<?php

use Densou\TradingDesk\Command\SearchHistory\AddToSearchHistoryCommand;
use Densou\TradingDesk\Query\SearchHistory\GetSortedSearchResultFromHistoryQuery;
use Densou\TradingDesk\Query\SearchHistory\RecentSearchesQuery;
use Densou\TradingDesk\Query\ExternalSource\Github\GithubRepositoryContributorsQuery;
use Densou\TradingDesk\Query\Sorter\Direction;
use Densou\TradingDesk\Query\Sorter\Order;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\RuntimeException;
use Prooph\ServiceBus\QueryBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->get('/searches/recent', function (Request $request) use ($app) {

    /** @var QueryBus $queryBus */
    $queryBus = $app['querybus'];

    $promise = $queryBus->dispatch(
        new RecentSearchesQuery($request->get('limit', $app['config']['recent_search.default_limit']))
    );

    $response = null;

    $promise->done(
        function ($recentSearches) use ($app, &$response) {

            $response = $app['twig']->render('recent.twig', [
                'recentSearches' => $recentSearches
            ]);
        },
        function (\Exception $e) use ($app, &$response) {

            $runtimeExceptionClass = RuntimeException::class;
            if ($e instanceof $runtimeExceptionClass) {
                $e = $e->getPrevious();
            }

            $response = $app['twig']->render('error.twig', ['message' => $e->getMessage()]);
        }
    );

    return $response;
});

$app->post('/searches', function (Request $request) use ($app) {

    /** @var QueryBus $queryBus */
    $queryBus = $app['querybus'];

    /** @var CommandBus $commandBus */
    $commandBus = $app['commandbus'];

    $searchText = $request->get('searchText');

    $promise = $queryBus->dispatch(
        new GithubRepositoryContributorsQuery(
            $searchText
        )
    );

    $response = null;

    $promise
        ->done(
            function ($searchResult) use ($app, &$response, $searchText, $commandBus) {

                $command = new AddToSearchHistoryCommand(Uuid::uuid4(), $searchText, $searchResult);
                $commandBus->dispatch(
                    $command
                );

                $response = $app->redirect($app['url_generator']->generate('search-history-item', [
                    'searchId' => $command->searchId()
                ]), Response::HTTP_CREATED);
            },
            function (\Exception $e) use ($app, &$response) {

                $runtimeExceptionClass = RuntimeException::class;
                if ($e instanceof $runtimeExceptionClass) {
                    $e = $e->getPrevious();
                }

                $response = $app['twig']->render('error.twig', ['message' => $e->getMessage()]);
            }
        );

    return $response;
});

$app->get('searches/{searchId}', function (Request $request, string $searchId) use ($app) {

    /** @var QueryBus $queryBus */
    $queryBus = $app['querybus'];

    $sortBy = $request->get('sortby');
    $direction = $request->get('dir', 'ASC');

    $order = $sortBy ? new Order($sortBy, new Direction(strtoupper($direction))) : null;

    $promise = $queryBus->dispatch(
        new GetSortedSearchResultFromHistoryQuery(Uuid::fromString($searchId), $order)
    );

    $response = null;

    $promise->done(
        function ($historySearchResult) use ($app, &$response, $sortBy, $direction) {

            $response = $app['twig']->render('search.twig', array_merge($historySearchResult,
                [
                    'sorters' => array_keys($app['app.sorter.comparators']['repository-contrinutors']),
                    'currentSorter' => $sortBy,
                    'currentSorterDir' => $direction
                ]
            ));

        },
        function (\Exception $e) use ($app, &$response) {

            $runtimeExceptionClass = RuntimeException::class;
            if ($e instanceof $runtimeExceptionClass) {
                $e = $e->getPrevious();
            }

            $response = $app['twig']->render('error.twig', ['message' => $e->getMessage()]);
        }
    );

    return $response;
})->bind('search-history-item');

$app->get('/', function () use ($app) {
    return $app['twig']->render('empty.twig');
});