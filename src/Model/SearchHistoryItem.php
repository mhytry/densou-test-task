<?php

namespace Densou\TradingDesk\Model;

use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

/**
 * SearchHistoryItem entity. Protects data integrity during modifications search of history
 *
 * @package Densou\TradingDesk\Model
 */
class SearchHistoryItem
{

    /**
     * @var UuidInterface
     */
    private $searchId;

    /**
     * @var string
     */
    private $searchText;

    /**
     * @var DateTimeImmutable
     */
    private $dateTime;

    /**
     * @var array
     */
    private $searchResult;


    /**
     * SearchHistoryItem constructor.
     * @param UuidInterface $searchId
     * @param string $searchText
     * @param array $searchResult
     * @param DateTimeImmutable $dateTime
     */
    public function __construct(UuidInterface $searchId, string $searchText, array $searchResult, DateTimeImmutable $dateTime)
    {
        $this->searchText = $searchText;
        $this->dateTime = $dateTime;
        $this->searchResult = $searchResult;
        $this->searchId = $searchId;
    }


    /**
     * @return DateTimeImmutable
     */
    public function dateTime(): DateTimeImmutable
    {
        return $this->dateTime;
    }


    /**
     * @return string
     */
    public function searchText(): string
    {
        return $this->searchText;
    }

    /**
     * @return UuidInterface
     */
    public function searchId(): UuidInterface
    {
        return $this->searchId;
    }

    /**
     * @return array
     */
    public function searchResult(): array
    {
        return $this->searchResult;
    }
}