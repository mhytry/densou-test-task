<?php

namespace Densou\TradingDesk\Command\SearchHistory;


use Densou\TradingDesk\Command\DateTimeProvider;
use Densou\TradingDesk\Model\SearchHistoryItem;

/**
 * Implementation of adding new search to the history of searches
 *
 * @package Densou\TradingDesk\Command\SearchHistory
 */
class AddToSearchHistoryCommandHandler
{

    /**
     * @var SearchHistoryWriterProvider
     */
    private $searchHistoryWriterProvider;

    /**
     * @var DateTimeProvider
     */
    private $dateTimeProvider;

    /**
     * AddToSearchHistoryCommandHandler constructor.
     * @param SearchHistoryWriterProvider $searchHistoryWriterProvider
     * @param DateTimeProvider $dateTimeProvider
     */
    public function __construct(
        SearchHistoryWriterProvider $searchHistoryWriterProvider,
        DateTimeProvider $dateTimeProvider
    )
    {
        $this->searchHistoryWriterProvider = $searchHistoryWriterProvider;
        $this->dateTimeProvider = $dateTimeProvider;
    }

    /**
     * Adds new search to the history of searches
     *
     * @param AddToSearchHistoryCommand $command
     */
    public function __invoke(AddToSearchHistoryCommand $command): void
    {
        $searchHistoryItem = new SearchHistoryItem(
            $command->searchId(),
            $command->searchText(),
            $command->searchResult(),
            $this->dateTimeProvider->current()
        );

        $this->searchHistoryWriterProvider->add($searchHistoryItem);
    }
}