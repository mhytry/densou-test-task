<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 13:27
 */

namespace Densou\TradingDesk\Command\SearchHistory;


use Densou\TradingDesk\Model\SearchHistoryItem;

/**
 * Allows modifications in history of searches
 *
 * @package Densou\TradingDesk\Command\SearchHistory
 */
interface SearchHistoryWriterProvider
{
    /**
     * Add new item to history of searches
     *
     * @param SearchHistoryItem $searchHistoryItem
     */
    public function add(SearchHistoryItem $searchHistoryItem): void;
}