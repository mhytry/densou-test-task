<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 13:18
 */

namespace Densou\TradingDesk\Command\SearchHistory;

use Ramsey\Uuid\UuidInterface;

/**
 * DTO
 *
 * @package Densou\TradingDesk\Command\SearchHistory
 */
class AddToSearchHistoryCommand
{

    /**
     * @var string
     */
    private $searchText;
    /**
     * @var string
     */

    private $searchId;
    /**
     * @var array
     */

    private $searchResult;

    /**
     * AddToSearchHistoryCommand constructor.
     * @param UuidInterface $searchId
     * @param string $searchText
     * @param array $searchResult
     */
    public function __construct(UuidInterface $searchId, string $searchText, array $searchResult)
    {
        $this->searchText = $searchText;
        $this->searchId = $searchId;
        $this->searchResult = $searchResult;
    }

    /**
     * Returns searchText
     * @return string
     */
    public function searchText(): string
    {
        return $this->searchText;
    }

    /**
     * Returns searchId
     * @return UuidInterface
     */
    public function searchId(): UuidInterface
    {
        return $this->searchId;
    }

    /**
     * Returns searchResult
     * @return array
     */
    public function searchResult(): array
    {
        return $this->searchResult;
    }
}