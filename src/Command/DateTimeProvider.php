<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 02.10.2017
 * Time: 00:49
 */

namespace Densou\TradingDesk\Command;


use DateTimeImmutable;

/**
 * One place to provide time objects.
 * Use this provider instead of creating time objects on your own (new DateTimeImmutable, new DateTime, etc.)
 *
 * @package Densou\TradingDesk\Command
 */
interface DateTimeProvider
{

    /**
     * Returns current date and time as a immutable object
     *
     * @return DateTimeImmutable
     */
    public function current(): DateTimeImmutable;
}