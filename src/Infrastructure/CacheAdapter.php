<?php

namespace Densou\TradingDesk\Infrastructure;

use Densou\TradingDesk\Query\CacheProvider;
use Moust\Silex\Cache\CacheInterface;

/**
 * Moust\Silex\Cache adapter to providing cache ability
 * @package Densou\TradingDesk\Infrastructure
 */
class CacheAdapter implements CacheProvider
{

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * Time to leave
     * @var int
     */
    private $defaultTtl;

    /**
     * CacheAdapter constructor.
     * @param CacheInterface $cache
     * @param int $defaultTtl
     */
    public function __construct(CacheInterface $cache, int $defaultTtl)
    {
        $this->cache = $cache;
        $this->defaultTtl = $defaultTtl;
    }

    /**
     * @inheritdoc
     */
    public function get(string $key): ?array
    {
        $cacheItem = $this->cache->fetch($key);
        return $cacheItem ?: null;
    }

    /**
     * @inheritdoc
     */
    public function set(string $key, array $data): void
    {
        $this->cache->store($key, $data, $this->defaultTtl);
    }
}