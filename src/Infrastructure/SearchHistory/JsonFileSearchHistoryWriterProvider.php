<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 29.09.2017
 * Time: 22:41
 */

namespace Densou\TradingDesk\Infrastructure\SearchHistory;

use Densou\TradingDesk\Command\SearchHistory\SearchHistoryWriterProvider;
use Densou\TradingDesk\Infrastructure\JsonFileStorage;
use Densou\TradingDesk\Infrastructure\JsonFileStorageException;
use Densou\TradingDesk\Model\SearchHistoryItem;

/**
 * Writer implementation of history of searches in json file
 *
 * @package Densou\TradingDesk\Infrastructure\SearchHistory
 */
class JsonFileSearchHistoryWriterProvider extends JsonFileStorage implements SearchHistoryWriterProvider
{

    /**
     * @inheritdoc
     */
    public function add(SearchHistoryItem $searchHistoryItem): void
    {
        $this->load();

        $this->data[$searchHistoryItem->searchId()->toString()] = [
            'id' => $searchHistoryItem->searchId()->toString(),
            'searchText' => $searchHistoryItem->searchText(),
            'result' => $searchHistoryItem->searchResult(),
            'datetime' => $searchHistoryItem->dateTime()->format(DATE_RFC3339),
        ];

        $this->updateStorage();
    }

    /**
     * Updates json file storage. Used after modifying actions
     */
    private function updateStorage(): void
    {
        $saveResult = file_put_contents($this->storageFilePath, json_encode($this->data));

        if ($saveResult === false) {
            throw new JsonFileStorageException("Unable to save storage file");
        }
    }
}