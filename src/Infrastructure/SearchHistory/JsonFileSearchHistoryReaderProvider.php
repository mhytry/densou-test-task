<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 29.09.2017
 * Time: 22:41
 */

namespace Densou\TradingDesk\Infrastructure\SearchHistory;

use Densou\TradingDesk\Infrastructure\JsonFileStorage;
use Densou\TradingDesk\Query\SearchHistory\SearchHistoryReaderProvider;
use OutOfBoundsException;
use Ramsey\Uuid\UuidInterface;

/**
 * Reader implementation of history of searches in json file
 *
 * @package Densou\TradingDesk\Infrastructure\SearchHistory
 */
class JsonFileSearchHistoryReaderProvider extends JsonFileStorage implements SearchHistoryReaderProvider
{

    /**
     * @inheritdoc
     */
    public function last(int $limit): array
    {
        $this->load();

        return array_reverse(array_slice($this->data, -$limit, $limit));
    }

    /**
     * @inheritdoc
     */
    public function get(UuidInterface $searchId): array
    {
        $this->load();

        if (!array_key_exists($searchId->toString(), $this->data)) {
            throw new OutOfBoundsException("Search result not found");
        }

        return  $this->data[$searchId->toString()];
    }
}