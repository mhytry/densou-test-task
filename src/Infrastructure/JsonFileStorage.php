<?php

namespace Densou\TradingDesk\Infrastructure;

/**
 * Common actions related with json file storage
 *
 * @package Densou\TradingDesk\Infrastructure
 */
abstract class JsonFileStorage
{

    /**
     * Array representation of empty storage
     */
    private const EMPTY_STORAGE = [];

    /**
     * @var string
     */
    protected $storageFilePath;

    /**
     * @var array|null
     */
    protected $data;

    /**
     * JsonFileStorage constructor.
     * @param string $storageFilePath
     */
    public function __construct(string $storageFilePath)
    {
        $this->storageFilePath = $storageFilePath;
    }

    /**
     * Loads data from file to memory
     */
    protected function load(): void
    {

        if (!is_null($this->data)) {
            return;
        }

        $this->createStorageFile();

        $storageFileContent = file_get_contents($this->storageFilePath);

        if (empty($storageFileContent)) {
            return;
        }

        $this->data = json_decode($storageFileContent, true);
    }

    /**
     * Creates json file to store data
     */
    private function createStorageFile(): void
    {

        $dir = dirname($this->storageFilePath);

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        if (false === file_exists($this->storageFilePath) && false === file_put_contents($this->storageFilePath, json_encode(self::EMPTY_STORAGE))) {
            throw new JsonFileStorageException("Storage file does not exist and can not be created");
        }

        $this->data = self::EMPTY_STORAGE;
    }
}