<?php

namespace Densou\TradingDesk\Infrastructure;

use DateTimeImmutable;
use Densou\TradingDesk\Command\DateTimeProvider;

/**
 * @inheritdoc
 */
class LocalDateTimeProvider implements DateTimeProvider
{
    /**
     * @inheritdoc
     */
    public function current(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}