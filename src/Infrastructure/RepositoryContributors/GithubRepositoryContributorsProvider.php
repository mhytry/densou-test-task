<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 12:12
 */

namespace Densou\TradingDesk\Infrastructure\RepositoryContributors;


use Densou\TradingDesk\Query\ExternalSource\ExternalSourceDataProvider;
use Github\Client;
use Github\ResultPager;

/**
 * KnpLabs Github API adapter to providing contributors of repository
 *
 * @package Densou\TradingDesk\Infrastructure\RepositoryContributors
 */
class GithubRepositoryContributorsProvider implements ExternalSourceDataProvider
{

    /**
     * @var Client
     */
    private $client;
    /**
     * @var ResultPager
     */
    private $pager;
    /**
     * @var GithubContributorMapper
     */
    private $mapper;
    /**
     * @var RepositoryNameSpecification
     */
    private $repositoryNameSpec;


    /**
     * GithubRepositoryContributorsProvider constructor.
     * @param Client $client
     * @param ResultPager $pager
     * @param GithubContributorMapper $mapper
     * @param RepositoryNameSpecification $repositoryNameSpec
     */
    public function __construct(
        Client $client,
        ResultPager $pager,
        GithubContributorMapper $mapper, RepositoryNameSpecification $repositoryNameSpec
    )
    {
        $this->client = $client;
        $this->pager = $pager;
        $this->mapper = $mapper;
        $this->repositoryNameSpec = $repositoryNameSpec;
    }

    /**
     * @inheritdoc
     */
    public function data(string $repositoryName): array
    {
        $this->repositoryNameSpec->isSatisfiedBy($repositoryName);

        return $this->mapper->map(
            $this->pager->fetchAll(
                $this->client->api('repo'),
                'contributors',
                explode("/", $repositoryName)
            )
        );
    }
}