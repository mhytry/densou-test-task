<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 30.09.2017
 * Time: 17:21
 */

namespace Densou\TradingDesk\Infrastructure\RepositoryContributors;


/**
 * Mapper github contributor structure to supported by application structure
 *
 * @package Densou\TradingDesk\Infrastructure\RepositoryContributors
 */
class GithubContributorMapper
{
    /**
     * @param array $contributors
     * @return array
     */
    public function map(array $contributors): array
    {
        return array_map(function ($contributor) {
            return [
                'name' => $contributor['login'],
                'avatar' => $contributor['avatar_url'],
                'contributions' => $contributor['contributions']
            ];
        }, $contributors);
    }
}