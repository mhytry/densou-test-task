<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 02.10.2017
 * Time: 09:02
 */

namespace Densou\TradingDesk\Infrastructure\RepositoryContributors;

/**
 * Specification of Github's repository name
 *
 * @package Densou\TradingDesk\Infrastructure\RepositoryContributors
 */
class GithubRepositoryNameSpecification implements RepositoryNameSpecification
{
    /**
     * @inheritdoc
     */
    public function isSatisfiedBy(string $repositoryName): bool
    {
        preg_match_all('/(\/)/', $repositoryName, $matches, PREG_SET_ORDER);

        return count($matches) === 1;
    }
}