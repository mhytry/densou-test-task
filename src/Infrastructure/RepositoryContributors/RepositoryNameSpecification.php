<?php
/**
 * Created by PhpStorm.
 * User: mhytry
 * Date: 02.10.2017
 * Time: 09:25
 */

namespace Densou\TradingDesk\Infrastructure\RepositoryContributors;


/**
 * Specification of repository name depends on source
 *
 * @package Densou\TradingDesk\Infrastructure\RepositoryContributors
 */
interface RepositoryNameSpecification
{
    /**
     * Validates repository name
     *
     * @param string $repositoryName
     * @return bool
     */
    public function isSatisfiedBy(string $repositoryName): bool;
}