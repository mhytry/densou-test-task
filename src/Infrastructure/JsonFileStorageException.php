<?php

namespace Densou\TradingDesk\Infrastructure;

use RuntimeException;

/**
 * Class JsonFileStorageException
 * @package Densou\TradingDesk\Infrastructure
 */
class JsonFileStorageException extends RuntimeException
{

}