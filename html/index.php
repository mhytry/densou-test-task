<?php

use Densou\TradingDesk\Application;

require_once __DIR__ . "/../vendor/autoload.php";

$app = new Application();
$app['debug'] = true;

require_once __DIR__ . '/../resources/config/dev.php';
require_once __DIR__ . '/../resources/config/querybus.php';
require_once __DIR__ . '/../resources/config/commandbus.php';
require_once __DIR__ . '/../src/Application/Api/definition.php';

$app->run();